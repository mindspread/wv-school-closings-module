<?php
/**
 * Helper class for WV School Closings Module module
 * 
 * @package    Joomla.WVNET
 * @subpackage Modules
 * @license        GNU/GPL, see LICENSE.php
 * mod_wvschoolclosings is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class modWVSchoolClosingsHelper
{
    /**
     * Retrieves the hello message
     *
     * @param array $params An object containing the module parameters
     * @access public
     */    
    public static function getFeed( $county )
    {
    $countyFeed = 'http://wvde.state.wv.us/closings/rss/'.$county;
    $xml = simplexml_load_file($countyFeed); 
    return $xml;
    }
}
?>